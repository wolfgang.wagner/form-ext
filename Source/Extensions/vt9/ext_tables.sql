#
# Table structure for table 'tx_vt9_mails'
#
CREATE TABLE tx_vt9_mails (
    uid int(11) unsigned DEFAULT 0 NOT NULL auto_increment,
   	pid int(11) DEFAULT 0 NOT NULL,
   	tstamp int(11) unsigned DEFAULT 0 NOT NULL,
   	crdate int(11) unsigned DEFAULT 0 NOT NULL,
   	deleted tinyint(4) unsigned DEFAULT 0 NOT NULL,
   	hidden tinyint(4) unsigned DEFAULT 0 NOT NULL,
   	sys_language_uid int(11) DEFAULT 0 NOT NULL,
   	PRIMARY KEY (uid),
   	KEY parent (pid),

    name tinytext,
    email tinytext,
    message mediumtext,
);
