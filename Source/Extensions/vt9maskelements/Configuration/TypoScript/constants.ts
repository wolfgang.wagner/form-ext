# cat=vt9maskelements/file; type=string; label=Path to template root (FE)
plugin.tx_vt9maskelements.view.templateRootPath =

# cat=vt9maskelements/file; type=string; label=Path to template partials (FE)
plugin.tx_vt9maskelements.view.partialRootPath =

# cat=vt9maskelements/file; type=string; label=Path to template layouts (FE)
plugin.tx_vt9maskelements.view.layoutRootPath =
