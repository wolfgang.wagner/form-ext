# Zoominar "Formulare in TYPO3 10 – Advanced" am 28.04.2021

Im Zoominar verwendetet TYPO3-Installation 

Direkt verwendbar mit Docker+DDEV.

1. Klone dieses Repository (oder download als zip)
2. ddev start
3. ddev composer install
4. ddev import-db < database.sql
5. ddev launch /typo3

Benutzername: admin\
Passwort: password
